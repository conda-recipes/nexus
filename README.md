# nexus conda recipe

Home: https://github.com/nexusformat/code

Package license: LGPL-2.1

Recipe license: BSD 2-Clause

Summary: NeXus is a common data format for neutron, x-ray, and muon science
